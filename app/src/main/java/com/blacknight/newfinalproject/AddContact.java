package com.blacknight.newfinalproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.UUID;

import io.realm.Realm;

public class AddContact extends AppCompatActivity {

    private Realm myRealm;
    private SessionManager session;
    private TextView firstname;
    private TextView lastname;
    private TextView phonenumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        this.myRealm = Realm.getDefaultInstance();
        session = new SessionManager(getApplicationContext());
        firstname = (TextView) findViewById(R.id.firstname_field);
        lastname = (TextView) findViewById(R.id.lastname_field);
        phonenumber = (TextView) findViewById(R.id.phonenumber_field);
    }

    public void GuardarContacto(View view) {
        if(firstname.getText().toString().isEmpty() || phonenumber.getText().toString().isEmpty()) {
            if(firstname.getText().toString().isEmpty()) {
                firstname.setError(this.getString(R.string.firstname_empty_error));
            }
            if(phonenumber.getText().toString().isEmpty()){
                phonenumber.setError(this.getString(R.string.phonenumber_empty_error));
            }
        }else{
            this.myRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Contact contact = realm.createObject(Contact.class, UUID.randomUUID().toString());
                    contact.setUserId("" + session.devolverUsuario().getUserName());
                    contact.setFirstName(firstname.getText().toString());
                    if(lastname.getText().toString().isEmpty()){
                        contact.setLastName("");
                    }else{
                        contact.setLastName(lastname.getText().toString());
                    }
                    contact.setPhoneNumber(phonenumber.getText().toString());
                }
            });
            System.out.println("Guardado exitoso!");

            AlertDialog alertDialog ;//= new AlertDialog.Builder(context2);
            alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(""+this.getString(R.string.contact_created));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                } });
            alertDialog.show();
        }
    }

    public void Cancel(View view) {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }
}
