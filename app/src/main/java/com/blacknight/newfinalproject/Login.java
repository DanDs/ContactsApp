package com.blacknight.newfinalproject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {

    private SessionManager session;
    private TextView userNameField;
    private TextView passwordField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionManager(getApplicationContext());
    }

    public void login(View view) {
        userNameField = (TextView) findViewById(R.id.user_text_field);
        passwordField = (TextView) findViewById(R.id.password_text_field);

        if(userNameField.getText().toString().equals("admin") && passwordField.getText().toString().equals("admin")){

            session.createUserLoginSession(userNameField.getText().toString(),
                    passwordField.getText().toString());

            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);

            finish();
        }else{
            if(userNameField.getText().toString().isEmpty() || passwordField.getText().toString().isEmpty()){
                if(userNameField.getText().toString().isEmpty()){
                    userNameField.setError(this.getString(R.string.username_empty));
                }
                if(passwordField.getText().toString().isEmpty()){
                    passwordField.setError(this.getString(R.string.password_empty));
                }
            }
            else {
                Toast.makeText(getApplicationContext(),
                        this.getString(R.string.login_error_text),
                        Toast.LENGTH_LONG).show();
                userNameField.setText("");
                passwordField.setText("");
            }
        }
    }


    public void SignUp(View view) {
        AlertDialog alertDialog2 ;
        alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setTitle(""+this.getString(R.string.future_Section));
        alertDialog2.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            } });
        alertDialog2.show();
    }
}
