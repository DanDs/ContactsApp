package com.blacknight.newfinalproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    SessionManager session;
    private TextView txtHi;
    Button btnLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new SessionManager(getApplicationContext());
        btnLogout = (Button) findViewById(R.id.btnLogout);
        txtHi = (TextView) findViewById(R.id.txt_hi);
        txtHi.setText(txtHi.getText()+" "+session.devolverUsuario().getUserName());
        Toast.makeText(getApplicationContext(),
                "User Login Status: " + session.isUserLoggedIn(),
                Toast.LENGTH_LONG).show();
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                session.logoutUser();
            }
        });
    }

    public void ShowPhone(View view) {
        String phoneNumber = "";
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                "tel",phoneNumber, null));
        startActivity(phoneIntent);
    }

    public void GoToContacts(View view) {
        Intent i = new Intent(view.getContext(), Contacts.class);
        startActivity(i);
    }

    public void AddContact(View view) {
        Intent i = new Intent(view.getContext(), AddContact.class);
        startActivity(i);
    }

    public void ViewPlaces(View view) {
        AlertDialog alertDialog2 ;
        alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setTitle(""+this.getString(R.string.future_Section));
        alertDialog2.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            } });
        alertDialog2.show();
    }
}
