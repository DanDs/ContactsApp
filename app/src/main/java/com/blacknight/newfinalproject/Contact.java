package com.blacknight.newfinalproject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Contact extends RealmObject{

    @PrimaryKey
    private String id;
    private String userId;

    private String firstName;
    private String phoneNumber;
    private String lastName;

    /*
    public Contact(String firstname, String lastName, String phoneNumber){
        this.firstname = firstname;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }
*/
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
