package com.blacknight.newfinalproject;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

public class SessionManager extends Application {
    //Shared Preferences
    SharedPreferences pref;
    //Editor for Shared Preferences
    Editor editor;
    //Sharedpref file name
    private static final String PREF_NAME = "FinalProject";
    //Context
    Context _context;
    //Shared pref mode
    int PRIVATE_MODE = 0;
    private String KEY_USER = "user";
    private String KEY_USUARIO = "usuario";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Constructor
    public SessionManager(Context context) {
        _context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void createUserLoginSession(String name, String email){
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_USERNAME, name);
        guardarUsuario(new User(name));

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }

    public boolean checkLogin(){
        // Check login status
        if(!this.isUserLoggedIn()){

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, Login.class);

            // Closing all the Activities from stack
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);

            return true;
        }
        return false;
    }

    public void logoutUser(){

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, Login.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }


    // Check for login
    public boolean isUserLoggedIn(){
        return pref.getBoolean(IS_USER_LOGIN, false);
    }


    public void saveUser(String username) {
        editor.putString(KEY_USER, username);
        editor.commit();
    }
    public String getUserName() {
        return pref.getString(KEY_USER, null);
    }

    public void guardarUsuario(User u) {
        Gson gson = new Gson();
        String user_json = gson.toJson(u);
        editor.putString(KEY_USUARIO, user_json);
        editor.commit();
    }

    public User devolverUsuario() {
        Gson gson = new Gson();
        User u = gson.fromJson(pref.getString(KEY_USUARIO,null), User.class);
        return u;
    }

}
