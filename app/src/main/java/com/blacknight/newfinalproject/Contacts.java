package com.blacknight.newfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class Contacts extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Realm myRealm;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ArrayList<Contact> contactsList = new ArrayList<>();
        this.myRealm = Realm.getDefaultInstance();
        session = new SessionManager(getApplicationContext());

        RealmResults<Contact> list = this.myRealm.where(Contact.class).findAll();
        for (Contact u : list) {
            System.out.println(u.getId() + " Nombre" + u.getFirstName());
            contactsList.add(u);
        }

        UserListAdapter userListAdapter = new UserListAdapter(this,contactsList);
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.recyclerView.setAdapter(userListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void GoBack(View view) {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }
}
