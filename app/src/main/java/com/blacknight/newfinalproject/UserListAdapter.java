package com.blacknight.newfinalproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.realm.Realm;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserListViewHolder> {

    private ArrayList<Contact> contactsList;
    private Context context;
    private String PhoneNumber;

        public static class UserListViewHolder extends RecyclerView.ViewHolder implements
                View.OnClickListener {
            private TextView txtFirstName;
            private TextView txtLastName;
            private TextView txtPhoneNumber;
            private Context context2;
            private Realm myRealm;

            public UserListViewHolder(Context c2, View itemView) {
                super(itemView);
                txtFirstName = (TextView) itemView.findViewById(R.id.txt_FirstName);
                txtLastName = ( TextView) itemView.findViewById(R.id.txt_LastName);
                txtPhoneNumber = ( TextView) itemView.findViewById(R.id.txt_PhoneNumber);
                this.context2 = c2;
                itemView.setOnClickListener(this);
                this.myRealm = Realm.getDefaultInstance();
            }

            public void onClick(View v) {
                AlertDialog alertDialog ;//= new AlertDialog.Builder(context2);
                alertDialog = new AlertDialog.Builder(context2).create();
                alertDialog.setTitle(""+ txtFirstName.getText() + txtLastName.getText());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, ""+context2.getString(R.string.call_text), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+txtPhoneNumber.getText()));
                        context2.startActivity(intent);

                } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, ""+context2.getString(R.string.cancel_text), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        //...
                }});

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,""+context2.getString(R.string.delete_contact_text), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(context2);

                        alert.setTitle(context2.getString(R.string.are_you_sure_text));
                        alert.setPositiveButton(context2.getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                myRealm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        Contact result = myRealm.where(Contact.class).equalTo("firstName", txtFirstName.getText().toString()).findFirst();
                                        //RealmResults<Message> result = realm.where(Message.class).equalTo(Message.USER_ID,userId).findAll();
                                        System.out.println("Nombre a borrar: " + result.getFirstName());
                                        result.deleteFromRealm();

                                        AlertDialog alertDialog2 ;//= new AlertDialog.Builder(context2);
                                        alertDialog2 = new AlertDialog.Builder(context2).create();
                                        alertDialog2.setTitle(""+context2.getString(R.string.contact_deleted));
                                        alertDialog2.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent i = new Intent(context2, Contacts.class);
                                                context2.startActivity(i);
                                            } });
                                        alertDialog2.show();
                                        System.out.println("Borrado");
                                    }
                                });
                            }
                        });

                        alert.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                        });
                        alert.show();

                    }});

                alertDialog.show();

                System.out.println("Intentando llamar a: " + txtPhoneNumber.getText().toString());
            }
        }

    public UserListAdapter(Context c, ArrayList<Contact> lista) {
        this.contactsList = lista;
        this.context = c;

    }

    @Override
    public UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact, parent, false);
        return new UserListAdapter.UserListViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(UserListViewHolder holder, int position) {
        final Contact usuario = contactsList.get(position);
        holder.txtFirstName.setText(usuario.getFirstName());
        holder.txtLastName.setText(usuario.getLastName());
        holder.txtPhoneNumber.setText(usuario.getPhoneNumber());

    }

    @Override
    public int getItemCount() {
        return this.contactsList.size();
    }
}